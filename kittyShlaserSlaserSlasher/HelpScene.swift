//
//  HelpScene.swift
//  kittyShlaserSlaserSlasher
//
//  Created by kevin travers on 3/15/16.
//  Copyright © 2016 BunnyPhantom. All rights reserved.
//

import UIKit
import SpriteKit
class HelpScene: SKScene {
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        let touch = touches.first! as UITouch
        let touchLocation = touch.locationInNode(self)
        let touchedNode = self.nodeAtPoint(touchLocation)
        if(touchedNode.name == "Back"){
            let newGameScene = GameStartScene(fileNamed:"GameStartScene")
            newGameScene!.scaleMode = scaleMode
            let transitionType = SKTransition.flipHorizontalWithDuration(1.0)
            view?.presentScene(newGameScene!,transition: transitionType)
        }
        
    }
}
