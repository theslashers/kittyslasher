//
//  GameOverScene.swift
//  kittyShlaserSlaserSlasher
//
//  Created by Gabrielle Winterton on 3/14/16.
//  Copyright © 2016 BunnyPhantom. All rights reserved.
//

import UIKit
import SpriteKit

class GameOverScene: SKScene {

    override func didMoveToView(view: SKView) {
        
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        let touch = touches.first! as UITouch
        let touchLocation = touch.locationInNode(self)
        let touchedNode = self.nodeAtPoint(touchLocation)
        if(touchedNode.name == "TryAgainButton"){
            let gameOverScene = GameScene(fileNamed:"GameScene")
            gameOverScene!.scaleMode = scaleMode
            let transitionType = SKTransition.flipHorizontalWithDuration(1.0)
            view?.presentScene(gameOverScene!,transition: transitionType)
        }
        if(touchedNode.name == "help"){
            let gameOverScene = HelpScene(fileNamed:"HelpScene")
            gameOverScene!.scaleMode = scaleMode
            let transitionType = SKTransition.flipHorizontalWithDuration(1.0)
            view?.presentScene(gameOverScene!,transition: transitionType)
        }
    
    }
}
