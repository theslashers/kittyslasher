//
//  GameScene.swift
//  kittyShlaserSlaserSlasher
//
//  Created by kevin travers on 3/3/16.
//  Copyright (c) 2016 BunnyPhantom. All rights reserved.

//still to do
//add sounds
//add claw animation

import SpriteKit
import AVFoundation

enum haveBadFish {
    case Never, Always, Default
}
//chain makes game faster as game goes on
enum SequenceType: Int {
    case OneNoBadFish, One, TwoWithOneBadFish, Two, Three, Four, Chain, FastChain
}

class GameScene: SKScene, SKPhysicsContactDelegate {
    //game varibales
    let maxPoints = 10
    
    //z postions
    enum objectZPositions: CGFloat {
        case background = 0
        case fish = 1
        case badfish = 2
        case slice = 3
    }
    
    
    //labels
    var scoreLabel = SKLabelNode()
    var score:Int = 0
    
    var livesLabel = SKLabelNode()
    var lives:Int = 3
    
    var highScoreLabel = SKLabelNode()
    var highScore:String = "0"
    
    //slahses
    //create slices the way i want it
    
    var activeSlash: SKShapeNode!
    //keep track of points
    var activeSlashArr = [CGPoint]()
    //keep track of all the little fishies
    var activeFish = [SKSpriteNode]()
    
    //chnaces of picking the type of fish
    var fishScoreArray:[Int] = [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,3,3,3,3,3,3,3,3,4,4,4,4,5,5,6]
    
    
    //create delay between rounds completed
    var roundDelay = 1.0
    //declare array hod game sequnce
    var sequence: [SequenceType]!
    //wherer we in game sqeunce
    var sequencePosition = 0
    //time fire between chain
    var chainDelay = 3.0
    //when all fish dead
    var nextSequenceQueued = true
    //game is over
    var gameEnded = false
    
    
    //sounds
    var backgroundMusic:AVAudioPlayer = AVAudioPlayer()
    var gameOverMusic:AVAudioPlayer = AVAudioPlayer()
    override func didMoveToView(view: SKView) {
        
        self.physicsWorld.contactDelegate = self

        //have gravity less so slower falling
        physicsWorld.gravity = CGVectorMake(0, -6)
        //have objects slower can change latter
        physicsWorld.speed = 0.85
        //set up labeles
        setUpScore()
        setUpLives()
        setUpSclices()
        setUpHighScore()
        //set up the sound
        setUpSound()
        backgroundMusic.play()
        
        sequence = [.OneNoBadFish, .OneNoBadFish, .TwoWithOneBadFish, .TwoWithOneBadFish, .Three, .One, .Chain]
        
        for _ in 0 ... 1000 {
            let nextSequence = SequenceType(rawValue: Int(arc4random_uniform(5)+2))!
            sequence.append(nextSequence)
        }
        
        RunAfterDelay(2) { [unowned self] in
            self.fishStart()
        }
        
    }
    
    override func update(currentTime: NSTimeInterval) {
        checkHighScore()
        if activeFish.count > 0 {
            for node in activeFish {
                if node.position.y < -140 {
                    node.removeAllActions()
                    //if it bad fish reomve it else lose a life
                    if node.name == "badFish" {
                        node.name = ""
                        node.removeFromParent()
                        
                        if let index = activeFish.indexOf(node) {
                            activeFish.removeAtIndex(index)
                        }
                    }else if node.name == "sushi" || node.name == "deadFish"{
                        node.name = ""
                        node.removeFromParent()
                        
                        if let index = activeFish.indexOf(node) {
                            activeFish.removeAtIndex(index)
                        }

                    }else{
                        node.name = ""
                        decrementLives()
                        
                        node.removeFromParent()
                        
                        if let index = activeFish.indexOf(node) {
                            activeFish.removeAtIndex(index)
                        }
                    }
                }
            }
        } else {
            
            if !nextSequenceQueued {
                
                RunAfterDelay(roundDelay) { [unowned self] in
                    self.fishStart()
                }
                
                nextSequenceQueued = true
            }
        }
        if lives <= 0 {
            gameOver()
        }
        
    }
    func gameOver(){
        gameOverMusic.play()
        let newGameScene = GameOverScene(fileNamed:"GameOverScene")
        newGameScene!.scaleMode = scaleMode
        let transitionType = SKTransition.flipHorizontalWithDuration(1.0)
        view?.presentScene(newGameScene!,transition: transitionType)
    }
    func updateScore(value:Int){
        score += value
        scoreLabel.text = "Score: \(score)"
    }
    func updateLivesLabel(){
        livesLabel.text = "Lives: \(lives)"
    }
    func decrementLives(){
        
        lives--
        updateLivesLabel()
    }
    func updateHighScoreLabel(){
        highScoreLabel.text = "HighScore: \(highScore)"
    }
    //set up score node and font size location
    func setUpScore() {
        scoreLabel = SKLabelNode(fontNamed: "Chalkduster")
        scoreLabel.text = "Score: 0"
        scoreLabel.horizontalAlignmentMode = .Left
        scoreLabel.fontSize = 20
        
        addChild(scoreLabel)
        
        scoreLabel.position = CGPoint(x:8 , y: 600)
    }
    //set up lives node and font size location
    func setUpLives(){
        livesLabel = SKLabelNode(fontNamed: "Chalkduster")
        livesLabel.text = "Lives: 3"
        livesLabel.horizontalAlignmentMode = .Left
        livesLabel.fontSize = 20
        
        addChild(livesLabel)
        
        livesLabel.position = CGPoint(x: 8, y: 625)
    }
    func setUpHighScore(){
        highScoreLabel = SKLabelNode(fontNamed: "Chalkduster")
        checkHighScore()
        highScoreLabel.text = "HighScore: \(highScore)"
        highScoreLabel.horizontalAlignmentMode = .Left
        highScoreLabel.fontSize = 20
        
        addChild(highScoreLabel)
        
        highScoreLabel.position = CGPoint(x: 8, y: 650)
    }
    //create slcing animation
    func setUpSclices() {
        activeSlash = SKShapeNode()
        activeSlash.zPosition = objectZPositions.slice.rawValue
        
        activeSlash.strokeColor = UIColor.whiteColor()
        activeSlash.lineWidth = 5
        
        addChild(activeSlash)
    }
    
    //lots of things need to implemented here
    //remove all points in arr
    //add this first to arr
    //remove all actions to slice(fade)
    //set z value
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        super.touchesBegan(touches, withEvent: event)
        //remove all old
        activeSlashArr.removeAll(keepCapacity: true)
        
        let touch = touches.first! as UITouch
        let touchLocation = touch.locationInNode(self)
        activeSlashArr.append(touchLocation)
        reDrawKittySlash()
        //incase still fading when user touches again
        activeSlash.removeAllActions()
        //visiable to user
        activeSlash.alpha = 1
        
        
    }
    //add new point to arr
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        let touch = touches.first! as UITouch
        let touchLocation = touch.locationInNode(self)
        activeSlashArr.append(touchLocation)
        
        reDrawKittySlash()
        let nodes = nodesAtPoint(touchLocation)
        
        for node in nodes {
            //check if touch the bad fish else it touched a good fish
            if (node.name == "badFish"){
                decrementLives()
                node.name = "deadFish"
                node.alpha = 0
                
                //play sound
            }else if(node.name == "sushi"){
                
            }else if(node.name != nil){
                let value = Int(node.name!)
                updateScore(2*value!)
                node.name = "sushi"
                let fish:SKSpriteNode = node as! SKSpriteNode
                fish.texture = SKTexture(imageNamed: "sushi\(value!)")
                
            }
        }
        
        //make a sound
        //find sound
        //only play sound after prevous sound is done
        //if(!activeSlash){playsound}
    }
    //need to end somthing went wrong
    override func touchesCancelled(touches: Set<UITouch>?, withEvent event: UIEvent?) {
        touchesEnded(touches!, withEvent: event)
    }
    //once user lifts fingur off screen have the line fade slowy instead of disapperaing
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        activeSlash.runAction(SKAction.fadeOutWithDuration(0.7))
    }
    
    //need to have atleast 2 locatin in arr else cant draw
    //if more then x(10) points then delete old one so dont get to long
    func reDrawKittySlash(){
        
        if(activeSlashArr.count < 2){
            //add 2 more slahses to have kitty claw animations
            activeSlash.path = nil
            
        }else{
            //remove the oldest points in array so line wont be to long
            while(activeSlashArr.count > maxPoints){
                activeSlashArr.removeAtIndex(0)
            }
            //lets me create own personal path
            var kittyPath = UIBezierPath()
            //start at oldest in path
            kittyPath.moveToPoint(activeSlashArr[0])
            //add points in arr to line
            for index in 1...activeSlashArr.count-1{
                
                kittyPath.addLineToPoint(activeSlashArr[index])
            }
            
            activeSlash.path = kittyPath.CGPath
        }
        
    }
    //early on dont eant to have bad fish need to be easy
    func createFeesh(badFish: haveBadFish = .Default){
        var feesh = SKSpriteNode()
        //create chnace of getting bad fish
        //1 in 6 chance being a badfish
        var fishType = Int(arc4random_uniform(6))
        
        if badFish == .Never {
            //then not a bad fish
            fishType = 1
        } else if badFish == .Always {
            //is a bad fish
            fishType -= 1
        }
        //normal everyday kind of fish
        // pick random image for the fish
        //play a sound when fish is created maybe
        if(fishType >= 1){
            //pick random fish texture
            let size:UInt32 = 63
            let randNum = fishScoreArray[Int(arc4random_uniform(size))]
            feesh = SKSpriteNode(imageNamed: "fish\(randNum).jpg")
            //this will determine which sushi it will turn into
            feesh.name = "\(randNum)"
            
        }else{
            feesh = SKSpriteNode()
            feesh = SKSpriteNode(imageNamed: "badFish.jpg")
            feesh.zPosition = objectZPositions.badfish.rawValue
            feesh.name = "badFish"
            
            
        }
        //give fish a random starting location
        fishLocation(feesh)
        //apply random force on fish
        fishJump(feesh)
        addChild(feesh)
        activeFish.append(feesh)
    }
    
    func fishLocation(fish:SKSpriteNode){
        let screen: CGRect = UIScreen.mainScreen().bounds
        let screenWidth:UInt32 = UInt32(screen.width)
        //let screenHeight = screen.height
        let randNum = Int(arc4random_uniform(screenWidth) + 50)
        fish.position.x = CGFloat(randNum)
        fish.position.y = 200
    }
    func fishJump(fish:SKSpriteNode){
        
        var randomVelocity = CGFloat(Int(arc4random_uniform(6))) / 2.0
        if(randomVelocity < 3){
            randomVelocity = -randomVelocity
        }else{
            randomVelocity = randomVelocity - 3
        }
        var randomXVelocity = 0
        var randomYVelocity = 0
        //give it X velocity depending where the fish spawns initialy
        
        if fish.position.x < 93 {
            randomXVelocity = Int(arc4random_uniform(7)+8)
        } else if fish.position.x < 186 {
            randomXVelocity = Int(arc4random_uniform(2)+3)
        } else if fish.position.x < 279{
            randomXVelocity = -Int(arc4random_uniform(2)+3)
        } else {
            randomXVelocity = -Int(arc4random_uniform(7)+8)
        }
        //create a random y push
        randomYVelocity = Int(arc4random_uniform(20)+5)
        
        fish.physicsBody = SKPhysicsBody(texture: fish.texture!, size: fish.texture!.size())
        fish.physicsBody!.velocity = CGVector(dx: randomXVelocity * 40, dy: randomYVelocity * 40)
        fish.physicsBody!.angularVelocity = randomVelocity
        fish.physicsBody!.collisionBitMask = 0
    }
    //create sequnce of events
    func fishStart(){
        //slowy increase values
        roundDelay *= 0.991
        chainDelay *= 0.99
        physicsWorld.speed *= 1.02
        
        let sequenceType = sequence[sequencePosition]
        
        switch sequenceType {
        case .OneNoBadFish:
            createFeesh(.Never)
            
        case .One:
            createFeesh()
            
        case .TwoWithOneBadFish:
            createFeesh(.Never)
            createFeesh(.Always)
            
        case .Two:
            createFeesh()
            createFeesh()
            
        case .Three:
            createFeesh()
            createFeesh()
            createFeesh()
            
        case .Four:
            createFeesh()
            createFeesh()
            createFeesh()
            createFeesh()
            
        case .Chain:
            createFeesh()
            
            RunAfterDelay(chainDelay / 5.0) { [unowned self] in self.createFeesh() }
            RunAfterDelay(chainDelay / 5.0 * 2) { [unowned self] in self.createFeesh() }
            RunAfterDelay(chainDelay / 5.0 * 3) { [unowned self] in self.createFeesh() }
            RunAfterDelay(chainDelay / 5.0 * 4) { [unowned self] in self.createFeesh() }
            
        case .FastChain:
            createFeesh()
            
            RunAfterDelay(chainDelay / 10.0) { [unowned self] in self.createFeesh() }
            RunAfterDelay(chainDelay / 10.0 * 2) { [unowned self] in self.createFeesh() }
            RunAfterDelay(chainDelay / 10.0 * 3) { [unowned self] in self.createFeesh() }
            RunAfterDelay(chainDelay / 10.0 * 4) { [unowned self] in self.createFeesh() }
        }
        
        
        sequencePosition += 1
        
        nextSequenceQueued = false
    }
    
    //foind on the interwebs
    func RunAfterDelay(delay: NSTimeInterval, block: dispatch_block_t) {
        let time = dispatch_time(DISPATCH_TIME_NOW, Int64(delay * Double(NSEC_PER_SEC)))
        dispatch_after(time, dispatch_get_main_queue(), block)
    }
    
    
    func checkHighScore(){
       
        let defaults = NSUserDefaults.standardUserDefaults()
        if defaults.objectForKey("HighScore") == nil{
            defaults.setInteger(score, forKey: "HighScore")
            highScore = "0"
        }else{
            var highScoreNum = defaults.objectForKey("HighScore") as! Int
            
            if Int(highScoreNum) < score{
                defaults.setInteger(score, forKey: "HighScore")
                highScoreNum = score
                
            }
            highScore = "\(highScoreNum)"
        }

    }
    func setUpSound(){
        let url: NSURL = NSBundle.mainBundle().URLForResource("gameMusic", withExtension: "mp3")!
        do{
            backgroundMusic = try AVAudioPlayer(contentsOfURL: url)
        }catch _{
            print("Error loading the background music")
        }
        
        backgroundMusic.numberOfLoops = -1
        backgroundMusic.prepareToPlay()
        let urlGameOver:NSURL = NSBundle.mainBundle().URLForResource("meow", withExtension: "mp3")!
        do{
            gameOverMusic = try AVAudioPlayer(contentsOfURL: urlGameOver)
        }catch _{
            print("Error loading the game over music")
        }
        gameOverMusic.numberOfLoops = 1
        gameOverMusic.prepareToPlay()
        
    }

    
    
    
}
